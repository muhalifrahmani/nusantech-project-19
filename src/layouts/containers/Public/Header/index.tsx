import globalRoutes from 'layouts/routes'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React from 'react'

function Header() {
  const router = useRouter()
  const { pathname } = router
  return (
    <nav
      className={`h-[75px] flex items-center justify-between px-[80px] relative z-10 ${
        pathname === '/aboutus' && 'transparent'
      }`}
    >
      <div>
        {pathname === '/aboutus' ? (
          <img src="./static/images/logo/logo-nusantech-white.svg" alt="" />
        ) : (
          <img src="./static/images/logo/logo-nusantech.svg" alt="" />
        )}
      </div>
      <div className="flex items-center mt-1 list-none gap-7">
        {globalRoutes.map(
          (menu) =>
            menu.navbar && (
              <Link href={menu.path} key={menu.path}>
                <li
                  className={`cursor-pointer font-medium py-1 ${
                    pathname === menu.path
                      ? 'text-[#DF1E36] border-b-[3px] border-[#DF1E36]'
                      : 'text-[#404258]'
                  } ${pathname === '/aboutus' && 'text-white border-white'}`}
                  aria-hidden
                >
                  {menu.name}
                </li>
              </Link>
            ),
        )}
      </div>
    </nav>
  )
}

export default Header
